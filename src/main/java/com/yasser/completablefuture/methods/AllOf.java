package com.yasser.completablefuture.methods;

import java.util.concurrent.CompletableFuture;

public class AllOf {

	public static void main(String... a) {

		CompletableFuture<Integer> future1 = CompletableFuture.supplyAsync(() -> 2);
		CompletableFuture<Integer> future2 = CompletableFuture.supplyAsync(() -> 3);
		CompletableFuture<Integer> future3 = CompletableFuture.supplyAsync(() -> 5);

		CompletableFuture<Void> allFutures = CompletableFuture.allOf(future1, future2, future3);

		allFutures.thenRun(() -> System.out.println("All futures completed."));

	}
}
