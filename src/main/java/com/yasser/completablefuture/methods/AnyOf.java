package com.yasser.completablefuture.methods;

import java.util.concurrent.CompletableFuture;

public class AnyOf {

	public static void main(String... a) {

		CompletableFuture<Integer> future1 = CompletableFuture.supplyAsync(() -> 2);
		CompletableFuture<Integer> future2 = CompletableFuture.supplyAsync(() -> 3);
		CompletableFuture<Integer> future3 = CompletableFuture.supplyAsync(() -> 5);

		CompletableFuture<Object> anyFuture = CompletableFuture.anyOf(future1, future2, future3);

		anyFuture.thenAccept(result -> System.out.println("Result: " + result));

	}
}
