package com.yasser.completablefuture.methods;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

public class CompleteOnTimeout {

	public static void main(String... a) {

		CompletableFuture<String> future = new CompletableFuture<>();

		CompletableFuture<String> resultFuture = future.completeOnTimeout("Default value", 3, TimeUnit.SECONDS);

		resultFuture.thenAccept(result -> System.out.println("Result: " + result));

	}
}
