package com.yasser.completablefuture.methods;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class Exceptionally {

	public static void main(String... a) throws ExecutionException, InterruptedException {

		CompletableFuture<Integer> future = CompletableFuture.supplyAsync(() -> {
			throw new RuntimeException("Exception occurred during computation");
		});

		future.exceptionally(ex -> 0);

		future.get();
	}
}
