package com.yasser.completablefuture.methods;

import java.util.concurrent.CompletableFuture;

public class Handle {

	public static void main(String... a) {

		CompletableFuture<Integer> future = CompletableFuture.supplyAsync(() -> {
			throw new RuntimeException("Exception occurred during computation");
		});

		CompletableFuture<Integer> handledFuture = future.handle((result, ex) -> {
			if (ex != null) {
				System.err.println("Exception occurred: " + ex.getMessage());
				return 0; // Provide a fallback value
			} else {
				return result;
			}
		});

		handledFuture.thenAccept(result -> System.out.println("Result: " + result));

	}
}
