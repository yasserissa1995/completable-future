package com.yasser.completablefuture.methods;

import java.util.concurrent.CompletableFuture;

public class RunAfterBoth {

	public static void main(String... a) {

		CompletableFuture<Integer> future1 = CompletableFuture.supplyAsync(() -> 2);
		CompletableFuture<Integer> future2 = CompletableFuture.supplyAsync(() -> 3);

		CompletableFuture<Void> runAfterBothFuture = future1.runAfterBoth(future2, () -> System.out.println("Both completed"));

		runAfterBothFuture.thenRun(() -> System.out.println("Run after both completed"));

	}
}
