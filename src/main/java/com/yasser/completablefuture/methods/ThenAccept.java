package com.yasser.completablefuture.methods;

import java.util.concurrent.CompletableFuture;

public class ThenAccept {

	public static void main(String... a) {

		CompletableFuture.supplyAsync(() -> 2)
						 .thenAccept(System.out::println);

	}
}
