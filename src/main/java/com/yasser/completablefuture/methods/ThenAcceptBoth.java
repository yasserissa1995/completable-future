package com.yasser.completablefuture.methods;

import java.util.concurrent.CompletableFuture;

public class ThenAcceptBoth {

	public static void main(String... a) {

		CompletableFuture<Integer> future1 = CompletableFuture.supplyAsync(() -> 2);
		CompletableFuture<Integer> future2 = CompletableFuture.supplyAsync(() -> 3);

		CompletableFuture<Void> acceptBothFuture = future1.thenAcceptBoth(future2,
				(result1, result2) -> System.out.println("Result: " + (result1 + result2)));

		acceptBothFuture.thenRun(() -> System.out.println("Accept both completed"));

	}
}
