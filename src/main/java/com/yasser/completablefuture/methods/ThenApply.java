package com.yasser.completablefuture.methods;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class ThenApply {

	public static void main(String... a) throws ExecutionException, InterruptedException {

		CompletableFuture<Integer> future = CompletableFuture.supplyAsync(() -> 2)
															 .thenApply(result -> result + 2)
															 .thenApply(result -> result + 2);

		future.get();
	}
}
