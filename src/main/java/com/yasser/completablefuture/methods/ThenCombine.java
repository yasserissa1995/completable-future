package com.yasser.completablefuture.methods;

import java.util.concurrent.CompletableFuture;

public class ThenCombine {

	public static void main(String... a) {

		CompletableFuture<Integer> future1 = CompletableFuture.supplyAsync(() -> 2);
		CompletableFuture<Integer> future2 = CompletableFuture.supplyAsync(() -> 3);

		CompletableFuture<Integer> combinedFuture = future1.thenCombine(future2, (result1, result2) -> result1 + result2);

		combinedFuture.thenAccept(System.out::println);

	}
}
