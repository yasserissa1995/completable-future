package com.yasser.completablefuture.methods;

import java.util.concurrent.CompletableFuture;

public class ThenCompose {

	public static void main(String... a) {

		CompletableFuture<Integer> future1 = CompletableFuture.supplyAsync(() -> 2);
		CompletableFuture<Integer> future2 = CompletableFuture.supplyAsync(() -> 3);

		CompletableFuture<Integer> composedFuture = future1.thenCompose(result1 -> future2.thenApply(result2 -> result1 * result2));

		composedFuture.thenAccept(System.out::println);
	}
}
