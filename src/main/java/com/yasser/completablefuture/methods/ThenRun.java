package com.yasser.completablefuture.methods;

import java.util.concurrent.CompletableFuture;

public class ThenRun {

	public static void main(String... a) {

		CompletableFuture<Integer> future = CompletableFuture.supplyAsync(() -> 2 * 3);

		CompletableFuture<Void> thenRunFuture = future.thenRun(System.out::println);

		thenRunFuture.thenRun(System.out::println);

	}
}
