package com.yasser.future.future;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class FutureClient {

	public static void main(String... a) throws ExecutionException, InterruptedException {

		ExecutorService executor = Executors.newFixedThreadPool(5);
		Future<Integer> future1 = executor.submit(() -> 5);

		Future<Integer> future2 = executor.submit(() -> future1.get() + 5); // Add 5 to the result

		Future<Integer> future3 = executor.submit(() -> future2.get() + 5); // Add 5 to the result

		future1.get();
		future2.get();
		future3.get();

	}
}
